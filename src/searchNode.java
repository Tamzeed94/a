
public class searchNode implements Comparable<searchNode>{

	private Board board;
	private int moves;
	private searchNode parent;
	private int priority;
	public searchNode()
	{
		this.setMoves(0);
		
	}
	public searchNode(Board board, searchNode parent)
	{
		this.board=board;
		this.parent=parent;
		if(this.parent==null)
		{
			this.setMoves(0);
		}
		else
		{
			this.setMoves(parent.getMoves()+1);
		}
		this.setPriority(this.board.hamming()+moves);
	}
	public Board getBoard() {
		return board;
	}
	public void setBoard(Board board) {
		this.board = board;
	}
	public int getMoves() {
		return moves;
	}
	public void setMoves(int moves) {
		this.moves = moves;
	}
	public searchNode getParent() {
		return parent;
	}
	public void setParent(searchNode parent) {
		this.parent = parent;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	@Override
	public int compareTo(searchNode o) {
		// TODO Auto-generated method stub
		//return this.priority-o.getPriority();
		int diff= this.priority- o.priority;
		if(diff<0)
			return -1;
		else if(diff>0)
			return 1;
		else 
			return 0;
	}
	
	
}
