import java.awt.List;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;


public class Board {
	int board[][];
	int linearBoard[];
	int boardSize=0;
	public Board(int[][] blocks)
	{
		int ind=0;
		boardSize= blocks.length;
		//System.out.println("length is: "+boardSize);
		board=new int[boardSize][boardSize];
		linearBoard=new int[boardSize*boardSize];
		for(int i=0;i<boardSize;i++)
		{
			for(int j=0;j<boardSize;j++)
			{
				board[i][j]=blocks[i][j];
				if(blocks[i][j]!=0)
					linearBoard[ind++]=blocks[i][j];
			}
			
		}	
		
	}
	public int size()
	{
		return boardSize;
	}
	
	public int[] findHole()
	{
		int ret[]=new int [2];
		for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				//System.out.println("b: "+board[i][j]+" "+i+" "+j);
				if(board[i][j]==0)
				{
					ret[0]=i;
					ret[1]=j;
					//System.out.println("i j:"+i+" "+j);
					break;
				}
			}
			
		}
		return ret;
	}
	
	public Iterable<Board> neighbors()
	{
		///
		ArrayList<Board> ls=new ArrayList<Board>();
		///
		int posX,posY,tempX,tempY;
		int ar[]=new int[2];
		//Iterable list=new Iterable<Board>();
		ar=findHole();
		posX=ar[0];
		posY=ar[1];
		//System.out.println("posx posy:"+ posX+" "+posY);
		for(int i=1;i<=4;i++){
			int f=0;
			Board temp=new Board(this.board);
			if(i==4)
			{
				tempX=posX-1;
				if(tempX>=0)
				{
					temp.board[posX][posY]=temp.board[tempX][posY];
					temp.board[tempX][posY]=0;
					f=1;
				}
				
				
			}
			else if(i==3)
			{
				tempX=posX+1;
				if(tempX<boardSize)
				{
					temp.board[posX][posY]=temp.board[tempX][posY];
					temp.board[tempX][posY]=0;
					f=1;
					
				}
				
			}
			else if(i==2)
			{
				tempY=posY-1;
				if(tempY>=0)
				{
					temp.board[posX][posY]=temp.board[posX][tempY];
					temp.board[posX][tempY]=0;
					f=1;
				}
				
			}
			else if(i==1)
			{
				tempY=posY+1;
				if(tempY<boardSize)
				{
					temp.board[posX][posY]=temp.board[posX][tempY];
					temp.board[posX][tempY]=0;
					f=1;
				}
				
			}
			if(f==1)
			{
				//System.out.println("I: "+i);
				ls.add(temp);
				
			}
				
		}
		
		return ls;
		
	}
	
    public int hamming()
    {
    	int ret=0;
    	int temp=1;
    	for(int i=0;i<boardSize;i++)
    	{
    		for(int j=0;j<boardSize;j++)
    		{
    			if(i==boardSize-1 & j==boardSize-1)
    			{
    				return ret;
    			}
    			if(board[i][j]!=temp)
    				ret++;
    			temp++;
    			
    			
    		}
    	}
    	return ret;
    }
    
    public boolean isGoal()
    {
    	int ret=0;
    	int temp=1;
    	for(int i=0;i<boardSize;i++)
    	{
    		for(int j=0;j<boardSize;j++)
    		{
    		
    			if(board[i][j]!=temp)
    			{
    				ret++;
    				break;
    				
    			}   				
    			temp++;
    			if(i==boardSize-1 & j==boardSize-2)
    			{
    				temp=0;
    			}
    			
    		}
    	}
    	if(ret==0)
    		return true;
    	return false;
    }
	
    public int inversion()
    {
    	int count=0;
    	int temp;
    	int sz=boardSize*boardSize;
    	for(int i=0;i<sz;i++)
    	{
    		temp=linearBoard[i];
    		for(int j=i+1;j<sz;j++)
    		{
    			if(linearBoard[j]>temp)
    			{
    				count++;
    			}
    		}
    	}
    	return count;
    	
    }
    
    public boolean isSolvable()
    {
    	int inv;
    	inv=inversion();
    	if(boardSize%2!=0)
    	{	
    		if(inv%2!=0)
    			return false;
    		else
    			return true;
    		
    		
    	}
    	else
    	{
    		if(inv%2==0)
    			return false;
    		else
    			return true;
    		
    	}
    	//return (Boolean) null;
    }
    
    public void printBoard(int board[][]){
    	for(int i=0;i<3;i++)
		{
			for(int j=0;j<3;j++)
			{
				
				System.out.print(board[i][j]);
				System.out.print(" ");
			}
			System.out.println("\n");
		}
    	
    }
	
    
    public boolean equals(Object y)
    {
    	if(y==null)
    		return false;
    	Board temp= (Board) y;
    	if(this.boardSize!=temp.boardSize)
    	{
    		return false;
    	}
    	for(int i=0;i<boardSize;i++)
    	{
    		for(int j=0;j<boardSize;j++)
    		{
    			if(this.board[i][j]!=temp.board[i][j])
    				return false;
    		}
    	}
    	
    	
    	return true;
    	
    }
    
    public String toString()
    {
    	String str="";
    	for(int i=0;i<this.boardSize;i++)
    	{
    		for(int j=0;j<this.boardSize;j++)
    		{
    			Integer temp=this.board[i][j];
    			str+=temp.toString();
    			//System.out.println("str: "+temp.toString()+" ");
    			str+=" ";
    		}
    		str+="\n";
    	}
		return str;
    	
    }
	
	
	public static void main(String args[]) throws Exception
	{
		int blocks[][]=new int[3][3];
		int temp=1;
		FileReader in = null;
    	in = new FileReader("input.txt");
    	int c=in.read();
    	//int blocks[][]=new int[3][3];
		//int temp=1;
		int i1=0,j1=0;
		while ((c = in.read()) != -1) {
			if(c!=32 & c!=10)
			{
				int tmp=c-48;
				System.out.println("read: "+Character.toString((char) c)+" "+tmp);
				blocks[i1][j1++]=tmp;
				if(j1>=3)
				{
					i1++;
					j1=0;
					
				}
				
			}
         }
		Board brd=new Board(blocks);
		
		//brd.printBoard(blocks);
		System.out.println(brd.toString());
		int tmp=brd.hamming();
		System.out.println("res: "+tmp);
		
		
		/////////
		System.out.println("printing out neighbours: ");
		ArrayList<Board> boards=(ArrayList<Board>) brd.neighbors();
		for(Board tempBoard:boards)
		{
			blocks=tempBoard.board;
			brd.printBoard(blocks);
			System.out.println("end");
			System.out.println("is goal"+tempBoard.isGoal()+" "+tempBoard.hamming());
		}
		
		
		
		
	}
   

}
