import java.io.FileReader;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.PriorityQueue;
import java.util.Stack;


public class Solver {
	private searchNode goalNode,rootNode;
	PriorityQueue<searchNode>pQueue;

	public Solver(Board initial)
	{
		pQueue=new PriorityQueue<searchNode>();
		this.rootNode=new searchNode(initial,null);
		pQueue.offer(rootNode);
		workerSolver();		
	}
	public void workerSolver()
	{
		searchNode currentNode=this.rootNode;
		Board currentBoard,parentBoard = null;
		while(true)
		{
			currentNode=pQueue.poll();
			System.out.println("node: "+currentNode.getBoard().toString());
			if(currentNode.getBoard().isGoal())
			{
				this.setGoalNode(currentNode);
				return;
			}
			currentBoard=currentNode.getBoard();
			if(currentNode.getParent()!=null)
				parentBoard=currentNode.getParent().getBoard();
			Iterable<Board> neighbours=currentBoard.neighbors();
			for(Board neighbour: neighbours)
			{
				System.out.println(neighbour.toString());
				System.out.println();
				if(currentNode.getParent()!=null)
				{
					boolean bool= neighbour.equals(parentBoard);
					if(bool == true)
					{
						//do nothing
					}
					else
					{
						pQueue.offer(new searchNode(neighbour,currentNode));
					}
				}
				else
				{
					pQueue.offer(new searchNode(neighbour,currentNode));
				}
				
			}
		}
		
	}
    public int moves()
    {
		return goalNode.getMoves();
    	
    }
    public searchNode getGoalNode() {
		return goalNode;
	}
	public void setGoalNode(searchNode goalNode) {
		this.goalNode = goalNode;
	}
	public Iterable<Board> solution()
    {
		ArrayList<Board> seq=new ArrayList<Board>();
		ArrayList<Board> seqReverse= new ArrayList<Board>();
		//seq.push(this.goalNode.getBoard());
		searchNode currNode=this.goalNode;
		while(currNode!=null)
		{
			seq.add(currNode.getBoard());
			currNode=currNode.getParent();		
		}
		for(int i=seq.size()-1;i>=0;i--)
		{
			seqReverse.add(seq.get(i));
		}
		
		return seqReverse;
    	
    }
	
    public static void main(String[] args) throws Exception
    {
    	
    	FileReader in = null;
    	in = new FileReader("input.txt");
    	int c=in.read();
    	int blocks[][]=new int[3][3];
		int temp=1;
		int i1=0,j1=0;
		while ((c = in.read()) != -1) {
			if(c!=32 & c!=10)
			{
				int tmp=c-48;
				System.out.println("read: "+Character.toString((char) c)+" "+tmp);
				blocks[i1][j1++]=tmp;
				if(j1>=3)
				{
					i1++;
					j1=0;
					
				}
				
			}
         }
		
		
    	Board initial = new Board(blocks);
    	if (initial.isSolvable()) {
    	Solver solver = new Solver(initial);
    	System.out.println("Minimum number of moves = " + solver.moves()); 
    	for (Board board : solver.solution())
    	            System.out.println(board);
    	}
    	// if not, report unsolvable
    	else {
    	System.out.println("Unsolvable puzzle");
    	}
    }

}
